let nameNode = document.getElementById("name");
let surnameNode = document.getElementById("surname");
let emailNode = document.getElementById("email");
let phoneNumberNode = document.getElementById("phoneNumber");

let validNumbers = "0123456789";
let validLetters = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
let validSigns = "!#$%&'*+-/=?^_`{|}~.@";

let submitButton = document.getElementById("submitButton");

submitButton.addEventListener("click", printDataValue);

function validateNameValue() {
  let nameValue = nameNode.value.trim();

  for (let i = 0; i < nameValue.length; i += 1) {
    if (validLetters.indexOf(nameValue[i]) == -1) {
      nameNode.focus();
      return false;
    }
  }
  return nameValue;
}

function validateSurnameValue() {
  let surnameValue = surnameNode.value.trim();

  for (let i = 0; i < surnameValue.length; i += 1) {
    if (validLetters.indexOf(surnameValue[i]) == -1) {
      surnameNode.focus();
      return false;
    }
  }
  return surnameValue;
}

function validateEmailValue() {
  let emailValue = emailNode.value.trim();

  for (i = 0; i < emailValue.length; i += 1) {
    if (
      (validLetters + validNumbers + validSigns).indexOf(emailValue[i]) == -1 ||
      (validLetters + validNumbers).indexOf(
        emailValue[emailValue.length - 5]
      ) == -1 ||
      emailValue.indexOf("@") != emailValue.lastIndexOf("@") ||
      emailValue[emailValue.length - 4] != "." ||
      emailValue[emailValue.length - 3] != "c" ||
      emailValue[emailValue.length - 2] != "o" ||
      emailValue[emailValue.length - 1] != "m"
    ) {
      emailNode.focus();
      return false;
    }
  }
  return emailValue;
}

function validatePhoneNumberValue() {
  let phoneNumberValue = phoneNumberNode.value.trim();

  for (let i = 0; i < phoneNumberValue.length; i += 1) {
    if (
      validNumbers.indexOf(phoneNumberValue[i]) == -1 ||
      phoneNumberValue[0] != 0 ||
      phoneNumberValue[1] == 0 ||
      phoneNumberValue.length != 10
    ) {
      phoneNumberNode.focus();
      return false;
    }
  }
  return phoneNumberValue;
}

function printDataValue() {
  if (
    validateNameValue() &&
    validateSurnameValue() &&
    validateEmailValue() &&
    validatePhoneNumberValue()
  ) {
    console.log("Name: " + validateNameValue());
    console.log("Surname: " + validateSurnameValue());
    console.log("Email: " + validateEmailValue());
    console.log("Phone number: " + validatePhoneNumberValue());
  } else {
    let nameErrMessage =
      Boolean(validateNameValue()) || "Name should contain only letters";
    let surnameErrMessage =
      Boolean(validateSurnameValue()) || "Surname should contain only letters";
    let emailErrMessage =
      Boolean(validateEmailValue()) || "Email should be abc@efg.com pattern";
    let phoneNumberErrMessage =
      Boolean(validatePhoneNumberValue()) ||
      'Phone Number should begin with "0", contain only numbers, and has 10 digits';
    let errArray = [
      nameErrMessage,
      surnameErrMessage,
      emailErrMessage,
      phoneNumberErrMessage
    ];

    for (i = 0; i < errArray.length; i += 1) {
      if (typeof errArray[i] == "string") {
        return alert(errArray[i]);
      }
    }
  }
}

// let nameNode = document.getElementById("name");
// let surnameNode = document.getElementById("surname");
// let emailNode = document.getElementById("email");
// let phoneNumberNode = document.getElementById("phoneNumber");

// let nameValue;
// let surnameValue;
// let emailValue;
// let phoneNumberValue;

// let submitButton = document.getElementById("submitButton");

// submitButton.addEventListener("click", printDataValue);

// function validateNameValue() {
//   nameValue = nameNode.value.trim();

//   if (/^[a-zA-Z]+$/.test(nameValue)) {
//     //do nothing;
//   } else {
//     alert("Name should contain only letters");
//     nameNode.focus();
//   }

//   return /^[a-zA-Z]+$/.test(nameValue);
// }

// function validateSurnameValue() {
//   surnameValue = surnameNode.value.trim();

//   if (/^[a-zA-Z]+$/.test(surnameValue)) {
//     //do nothing;
//   } else {
//     alert("Surname should contain only letters");
//     surnameNode.focus();
//   }

//   return /^[a-zA-Z]+$/.test(surnameValue);
// }

// function validateEmailValue() {
//   emailValue = emailNode.value.trim();

//   if (/\S+@\S+[0-9a-zA-Z]\.com$/.test(emailValue) && !/ /.test(emailValue)) {
//     //do nothing;
//   } else {
//     alert("Email should be abc@efg.com pattern");
//     emailNode.focus();
//   }

//   return /\S+@\S+[0-9a-zA-Z]\.com$/.test(emailValue) && !/ /.test(emailValue);
// }

// function validatePhonNumberValue() {
//   phoneNumberValue = phoneNumberNode.value.trim();

//   if (/^0[1-9][0-9]{8}$/.test(phoneNumberValue)) {
//     //do nothing;
//   } else {
//     alert(
//       'Phone number should begin with "0", contain only numbers, and has 10 digits'
//     );
//     phoneNumberNode.focus();
//   }

//   return /^0[1-9][0-9]{8}$/.test(phoneNumberValue);
// }

// function printDataValue() {
//   if (
//     validateNameValue() &&
//     validateSurnameValue() &&
//     validateEmailValue() &&
//     validatePhonNumberValue()
//   ) {
//     console.log("Name: " + nameValue);
//     console.log("Surname: " + surnameValue);
//     console.log("Email: " + emailValue);
//     console.log("Phone Number: " + phoneNumberValue);
//   } else {
//     //    alert("There's something worng");
//   }
// }
